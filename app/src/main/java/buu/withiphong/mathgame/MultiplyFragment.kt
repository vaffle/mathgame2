package buu.withiphong.mathgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.withiphong.mathgame.databinding.FragmentMultiplyBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MultiplyFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultiplyFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentMultiplyBinding
    private var param1: String? = null
    private var param2: String? = null
    private var answer = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_multiply, container, false)
        val args: MultiplyFragmentArgs = MultiplyFragmentArgs.fromBundle(requireArguments())
        var correct = args.correct
        var incorrect = args.incorrect
        binding.apply {
            setScore(correct, incorrect)
            generateQuest()

            btnAns1.setOnClickListener {
                val checkAns = btnAns1.text.toString()
                if (checkAns.toInt() == answer) {
                    Toast.makeText(activity, "correct", Toast.LENGTH_SHORT).show()
                    correct++
                    textCorrect.text = getString(R.string.scoreCorrect, correct)
                } else {
                    Toast.makeText(activity, "incorrect", Toast.LENGTH_SHORT).show()
                    incorrect++
                    textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
                }
                generateQuest()
            }
            btnAns2.setOnClickListener {
                val checkAns = btnAns2.text.toString()
                if (checkAns.toInt() == answer) {
                    Toast.makeText(activity, "correct", Toast.LENGTH_SHORT).show()
                    correct++
                    textCorrect.text = getString(R.string.scoreCorrect, correct)
                } else {
                    Toast.makeText(activity, "incorrect", Toast.LENGTH_SHORT).show()
                    incorrect++
                    textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
                }
                generateQuest()
            }

            btnAns3.setOnClickListener {
                val checkAns = btnAns3.text.toString()
                if (checkAns.toInt() == answer) {
                    Toast.makeText(activity, "correct", Toast.LENGTH_SHORT).show()
                    correct++
                    textCorrect.text = getString(R.string.scoreCorrect, correct)
                } else {
                    Toast.makeText(activity, "incorrect", Toast.LENGTH_SHORT).show()
                    incorrect++
                    textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
                }
                generateQuest()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this){
            view?.findNavController()?.navigate(MultiplyFragmentDirections.actionMultiplyFragmentToTitleFragment(correct, incorrect))
        }
        return binding.root
    }
    private fun FragmentMultiplyBinding.generateQuest() {
        val ranNum1 = (1..10).random()
        val ranNum2 = (1..10).random()
        num1.text = ranNum1.toString()
        num2.text = ranNum2.toString()
        val ans = ranNum1 * ranNum2
        answer = ans
        generateAns()
    }
    private fun FragmentMultiplyBinding.generateAns() {
        val answers = arrayOf(
            "btnAns1", "btnAns2", "btnAns3"
        )
        val btnRandom = answers[(0..2).random()]
        if (btnRandom == "btnAns1") {
            btnAns1.text = answer.toString()
            btnAns2.text = (0..100).random().toString()
            btnAns3.text = (0..100).random().toString()
        } else if (btnRandom == "btnAns2") {
            btnAns1.text = (0..100).random().toString()
            btnAns2.text = answer.toString()
            btnAns3.text = (0..100).random().toString()
        } else {
            btnAns1.text = (0..100).random().toString()
            btnAns2.text = (0..100).random().toString()
            btnAns3.text = answer.toString()
        }
    }
    private fun FragmentMultiplyBinding.setScore(
        correct: Int,
        incorrect: Int
    ) {
        textCorrect.text = getString(R.string.scoreCorrect, correct)
        textIncorrect.text = getString(R.string.scoreIncorrect, incorrect)
    }

}